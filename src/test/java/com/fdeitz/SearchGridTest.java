package com.fdeitz;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SearchGridTest {

    @Test
    public void testFindWords() {
        char[][] characterGrid = {
                {'H', 'A', 'S', 'D', 'F'},
                {'G', 'E', 'Y', 'B', 'H'},
                {'J', 'K', 'L', 'Z', 'X'},
                {'C', 'V', 'B', 'L', 'N'},
                {'G', 'O', 'O', 'D', 'O'}
        };

        List<String> wordsToSearch = new ArrayList<>(Arrays.asList("HELLO", "GOOD", "BYE"));

        SearchGrid searchGrid = new SearchGrid();
        List<String> results = searchGrid.findWords(characterGrid, wordsToSearch);

        Assert.assertEquals(3, results.size());

        String result1 = "HELLO 0:0 4:4";
        String result2 = "GOOD 4:0 4:3";
        String result3 = "BYE 1:3 1:1";

        Assert.assertEquals(result1, results.get(0));
        Assert.assertEquals(result2, results.get(1));
        Assert.assertEquals(result3, results.get(2));
    }
}
