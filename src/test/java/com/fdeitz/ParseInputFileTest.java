package com.fdeitz;

import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class ParseInputFileTest {

    private static final String TEST_FILE = Paths.get("src", "test", "resources", "Test1.txt").toString();
    private static final char[][] EXPECTED_GRID = {
            {'H', 'A', 'S', 'D', 'F'},
            {'G', 'E', 'Y', 'B', 'H'},
            {'J', 'K', 'L', 'Z', 'X'},
            {'C', 'V', 'B', 'L', 'N'},
            {'G', 'O', 'O', 'D', 'O'}
    };
    private static final List<String> EXPECTED_WORDS = Arrays.asList("HELLO", "GOOD", "BYE");

    @Test
    public void testGetCharacterGrid() {
        ParseInputFile parser = new ParseInputFile();
        parser.processFile(TEST_FILE);
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                Assert.assertArrayEquals(EXPECTED_GRID[i], parser.getCharacterGrid()[i]);
            }
        }
    }

    @Test
    public void testGetWords() {
        ParseInputFile parser = new ParseInputFile();
        parser.processFile(TEST_FILE);
        Assert.assertEquals(EXPECTED_WORDS, parser.getWords());
    }

}