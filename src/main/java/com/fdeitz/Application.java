package com.fdeitz;

import org.apache.commons.cli.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

/**
 * This is the Application class which includes the main method
 * that is the entry point to the alphabet-soup application
 * 
 * @author Fred Deitz
 */

@SpringBootApplication
public class Application {

    private static final String ARG_HELP = "help";
    private static final String ARG_FILE = "file";

    public static void main(String[] args) {
        String filename = null;
        Options options = createOptions();
        CommandLineParser parser = new DefaultParser();

        try {
            CommandLine commandLine = parser.parse(options, args);

            if (commandLine.hasOption(ARG_FILE)) {
                filename = commandLine.getOptionValue(ARG_FILE);
            } else {
                displayHelp(options);
            }
        } catch (ParseException e) {
            System.out.println("Error parsing arguments");
            displayHelp(options);
        }

        ParseInputFile parseFile = new ParseInputFile();
        parseFile.processFile(filename);
        SearchGrid wordSearch = new SearchGrid();
        List<String> results = wordSearch.findWords(parseFile.getCharacterGrid(), parseFile.getWords());

        // Print the results
        for (String result : results) {
            System.out.println(result);
        }

        System.exit(0);
    }

    private static void displayHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar alphabet-soup.jar", options);

        System.exit(0);
    }

    private static Options createOptions() {
        Options options = new Options();

        options.addOption(
                Option.builder("h")
                        .longOpt(ARG_HELP)
                        .desc("Display this help.")
                        .argName("help")
                        .build()
        );

        options.addOption(
                Option.builder("f")
                        .longOpt(ARG_FILE)
                        .desc("ASCII text file containing the word search board along with the words that need to be found.")
                        .argName("filename")
                        .hasArg()
                        .build()
        );

        return options;
    }
}


