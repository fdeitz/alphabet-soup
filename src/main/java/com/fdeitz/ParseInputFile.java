package com.fdeitz;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The ParseInputFile class reads in the input file
 * and parses it to get the character grid to be searched
 * and the words to search for
 *
 * @author Fred Deitz
 */

public class ParseInputFile {
    private final List<String> words = new ArrayList<>();
    private char[][] characterGrid;
    private int numRows;

    /**
    * Reads a file with the given filename using the {@link #readFile} method.
    * @param filename the name of the file to be processed
    */
    public void processFile(String filename) {
        readFile(filename);
    }


    /**
     * Reads a file given its filename, and processes each line in the file by calling processLine method.
     * @param filename the name of the file to be read
     * @throws FileNotFoundException if the specified file cannot be found
     * @throws IOException if an I/O error occurs while reading the file
     */
    private void readFile(String filename) {
        try {
            File file = new File(filename);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedRead = new BufferedReader(fileReader);

            int lineNumber = 0;
            String line;

            while ((line = bufferedRead.readLine()) != null) {
                lineNumber++;
                processLine(lineNumber, line);
            }

            fileReader.close();
            bufferedRead.close();
        } catch (FileNotFoundException e) {
            System.out.println("File " + filename + " not found");
            e.printStackTrace();
        } catch (IOException ie) {
            System.out.println("I/O Exception");
            ie.printStackTrace();
        }
    }

    /**
     * Processes a line of text from the file being read.
     * @param lineNumber the line number of the line being processed
     * @param line the text content of the line being processed
     */
    private void processLine(int lineNumber, String line) {
        if (lineNumber == 1) { // process first line which specifies number of rows x number of columns
            String[] gridDimensions = line.split("x");
            numRows = Integer.parseInt(gridDimensions[0]);
            int numColumns = Integer.parseInt(gridDimensions[1]);
            characterGrid = new char[numRows][numColumns];
        } else if (lineNumber <= numRows + 1) { //word grid from line 2 to number of rows + 1
            // Get rid of space between characters in each row
            String row = line.replace(" ", "");
            // Convert row string to array of chars
            char[] rowLetters = row.toCharArray();
            // grid row 0 number is line number - 2
            System.arraycopy(rowLetters, 0, characterGrid[lineNumber - 2], 0, rowLetters.length);
        } else { // the rest of the lines specify words to be searched - remove spaces
            words.add(line.replaceAll("\\s", ""));
        }
    }

    public List<String> getWords() {
        return words;
    }

    public char[][] getCharacterGrid() {
        return characterGrid;
    }
}
