package com.fdeitz;

import java.util.ArrayList;
import java.util.List;

/**
 * The SearchGrid class searches a character grid
 * for a list of words and provides a list of results
 * in the specified output format
 *
 * @author Fred Deitz
 */

public class SearchGrid {
    private int endRow;
    private int endColumn;

    /**
     * Searches for given words in a character grid and returns their locations as a list of strings.
     * Each string in the returned list is formatted as "word startRow:startColumn endRow:endColumn".
     * @param characterGrid the 2D character array representing the grid to search
     * @param wordsToSearch the list of words to search for in the grid
     * @return a list of strings containing the locations of each word found in the grid
     */
    public List<String> findWords(char[][] characterGrid, List<String> wordsToSearch) {
        List<String> results = new ArrayList<>();
        int numRows = characterGrid.length;
        int numColumns = characterGrid[0].length;

        // Iterate over each word in the list
        for (String word : wordsToSearch) {
            for (int i = 0; i < numRows; i++) {
                for (int j = 0; j < numColumns; j++) {
                    // Check each position in the characterGrid for the first letter of the word
                    if (characterGrid[i][j] == word.charAt(0)) {
                        // Check all eight directions for the word
                        for (int rowIncrement = -1; rowIncrement <= 1; rowIncrement++) {
                            for (int columnIncrement = -1; columnIncrement <= 1; columnIncrement++) {
                                // Only execute checkDirection method if there is a search in at least one direction
                                if (!(rowIncrement ==0 && columnIncrement == 0)) {
                                    if (checkDirection(characterGrid, word, i, j, rowIncrement, columnIncrement)) {
                                        results.add(word + " " + i + ":" + j + " " + endRow + ":" + endColumn);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return results;
    }


    /**
     * Checks if a given word can be found in a character grid in a specific direction.
     * @param characterGrid the character grid to search in.
     * @param word the word to search for.
     * @param row the row index of the starting position.
     * @param column the column index of the starting position.
     * @param rowInc the amount to increment the row index for each step in the given direction.
     * @param columnInc the amount to increment the column index for each step in the given direction.
     * @return true if the word is found in the given direction, false otherwise.
     */
    private boolean checkDirection(char[][] characterGrid, String word, int row, int column, int rowInc, int columnInc) {
        int wordIndex = 1;
        int numRows = characterGrid.length;
        int numColumns = characterGrid[0].length;

        // Check if the word fits in the direction
        while (wordIndex < word.length() && (row + rowInc >= 0) && (row + rowInc < numRows) &&
                (column + columnInc >= 0) && (column + columnInc < numColumns)) {
            if (characterGrid[row + rowInc][column + columnInc] == word.charAt(wordIndex)) {
                row += rowInc;
                column += columnInc;
                wordIndex++;
                endRow = row;
                endColumn = column;
            } else {
                return false;
            }
        }

        return wordIndex == word.length();
    }
}
